@extends('layouts.home')

@section('home')
    <div class="banner-container">
        <div class="login-container">
            <h1 class="login-title">Student Login</h1>
            <form>
                <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="submit-container">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <div class="courses-container">
        <div>
            <div class="container">
                <div class="row justify-content-center align-items-center course-header-container">
                    <h1 class="course-header">Our Courses</h1>
                </div>
            </div>
            <div class="courses-item-container container">
                <div class="row no-gutters text-center justify-content-center align-items-center">
                    <div class="col course-item">
                        <h3>Information Technology</h3>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <div class="learn-more-container">
                            <button class="btn btn-block btn-lg btn-primary">
                                Learn more
                            </button>
                        </div>
                    </div>
                    <div class="col course-item">
                        <h3>Information Technology</h3>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <div class="learn-more-container">
                            <button class="btn btn-block btn-lg btn-primary">
                                Learn more
                            </button>
                        </div>
                    </div>
                    <div class="col course-item">
                        <h3>Information Technology</h3>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <div class="learn-more-container">
                            <button class="btn btn-block btn-lg btn-primary">
                                Learn more
                            </button>
                        </div>
                    </div>
                    <div class="col course-item">
                        <h3>Information Technology</h3>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <p class="course-description">
                            aasefsafasefasefasefsaefsafsafsafsf
                        </p>
                        <div class="learn-more-container">
                            <button class="btn btn-block btn-lg btn-primary">
                                Learn more
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-us-container container text-center">
        <div class="about-us-title-container">
            <h1>adawdawd</h1>
        </div>
        <div class="row about-us-description">
            <p>awdaadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdwdawd</p>
            <p>awdaadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdadawdawdwdawd</p>
        </div>
    </div>
    <div class="news-letter-container">
        <div class="container">
            <div class="row no-gutters justify-content-center align-items-center">
                    <div class="col-5">
                        <p>Stay connected</p>
                        <div class="social-icons-container container">
                            <div class="row no-gutters">
                                <div class="social-icon-item">
                                    <img src="/img/ftr-soc-fb.png" alt=""/>
                                </div>
                                <div class="social-icon-item">
                                    <img src="/img/ftr-soc-fb.png" alt=""/>
                                </div>
                                <div class="social-icon-item">
                                    <img src="/img/ftr-soc-fb.png" alt=""/>
                                </div>
                                <div class="social-icon-item">
                                    <img src="/img/ftr-soc-fb.png" alt=""/>
                                </div>
                                <div class="social-icon-item">
                                    <img src="/img/ftr-soc-fb.png" alt=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <p>Sign up to our news letter</p>
                        <div class="container-fluid news-letter-form-container">
                            <div class="row no-gutters">
                                <div class="col-4 news-letter-input">
                                        <div class="form-group">
                                        <input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                </div>
                                <div class="col-4 news-letter-input">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                </div>
                                <div class="col news-letter-input">
                                    <div class="form-group">
                                            <button type="submit" class="btn sign-up-btn btn-primary btn-block">Sign up</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
@endsection 